from django.apps import AppConfig


class T6Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't6'
